﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Graphical_Assignment
{
    public class Rectangle:Shape
    {
        /// <summary>
        /// declare variables
        /// </summary>
        int height, width;


        /// <summary>
        /// Parameter constructor
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="height"></param>
        /// <param name="width"></param>
        public Rectangle(int x, int y, int height, int width) : base(x, y)
        {
            this.height = height;
            this.width = width;
        }

        /// <summary>
        /// another parameter constructor
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public Rectangle(int x, int y) : base(x, y)
        {

        }
        
        /// <summary>
        /// 
        /// Default constructor
        /// </summary>
        public Rectangle()
        {
        }



        /// <summary>
        /// draw method
        /// </summary>
        /// <param name="g"></param>
        /// <param name="c"></param>
        /// <param name="thickness"></param>
        /// <param name="br"></param>

        public override void Draw(Graphics g, Color c, int thickness,Brush br)
        {
            Pen p = new Pen(c, thickness);
            g.DrawRectangle(p, x, y, height, width);
          //  g.FillRectangle(br, x, y, height, width);
        }


        /// <summary>
        /// height setter
        /// </summary>
        /// <param name="height"></param>
        public void setHeight(int height)
        {
            this.height = height;
        }


        /// <summary>
        /// height getter
        /// </summary>
        /// <returns></returns>
        public int getHeight()
        {
            return this.height;
        }

        /// <summary>
        /// width setter
        /// </summary>
        /// <param name="width"></param>
        public void setWidth(int width)
        {
            this.width = width;
        }


        /// <summary>
        /// Width Getter
        /// </summary>
        /// <returns>width value</returns>
        public int getWidth()
        {
            return this.width;
        }
        



    }
}