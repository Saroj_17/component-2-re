﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;


namespace Graphical_Assignment
{
    class Square : Shape
    {
        /// <summary>
        /// declare variables
        /// </summary>
        int size;

        /// <summary>
        /// Parameter constructor
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public Square( int x, int y) 
        {
           
        }

        /// <summary>
        /// over-ride Shape class draw
        /// </summary>
        /// <param name="g"></param>
        /// <param name="c"></param>
        /// <param name="thickness"></param>
        /// <param name="br"></param>
        public override void Draw(Graphics g, Color c, int thickness,Brush br)
        {
            Pen p = new Pen(c, thickness);
            g.DrawRectangle(p, x, y, size,size);
       //     g.FillRectangle(br, x, y, size, size);
        }

        /// <summary>
        /// another parameter constructor
        /// </summary>
        public Square():base()
        {


        }
        /// <summary>
        /// set square area
        /// </summary>
        /// <param name="len"></param>
        public void SetSquare(int len)
        {
            this.size = len;
        }
        /// <summary>
        /// get square area
        /// </summary>
        /// <returns></returns>
        public int GetSquare()
        {
            return this.size;
        }

       
    }
}
