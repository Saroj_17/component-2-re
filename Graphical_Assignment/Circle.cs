﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Graphical_Assignment
{
    /// <summary>
    /// Circle class
    /// </summary>
    class Circle : Shape
    {

        /// <summary>
        /// declare variables
        /// </summary>
        int X, Y,rad;


        /// <summary>
        /// Parameter constructor
        /// </summary>
        /// <param name="X"></param>
        /// <param name="Y"></param>
        /// <param name="Rad"></param>
        public Circle(int X, int Y,int Rad) 
        {
            this.X = X;
            this.Y = Y;
            this.rad = Rad;
        }

        /// <summary>
        /// another parameter constructor
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public Circle(int x, int y) : base(x, y)
        {

        }

        /// <summary>
        /// default constructor
        /// </summary>
        public Circle()
        {


        }



        /// <summary>
        /// draw method
        /// </summary>
        /// <param name="g"></param>
        /// <param name="c"></param>
        /// <param name="thickness"></param>
        /// <param name="br"></param>

        public override void Draw(Graphics g, Color c, int thickness,Brush br)
        {
            Pen p = new Pen(c, thickness);
            
            g.DrawEllipse(p, x, y, rad,rad);
         ///   g.FillEllipse(br, x, y, rad, rad);
        }
        /// <summary>
        /// set radious if circle
        /// </summary>
        /// <param name="rad"></param>
        public void SetRadious(int rad)
        {
            this.rad = rad;
        }
        /// <summary>
        /// get radious
        /// </summary>
        /// <returns> radious</returns>
        public int getRadious()
        {
            return this.rad;
        }

        
       

    }
}
