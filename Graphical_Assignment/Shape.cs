﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Graphical_Assignment
{
    public abstract class Shape : ShapeInterface
    {
        /// <summary>
        /// Variable Declare
        /// </summary>
        protected Color colour;
        protected int x = 0, y = 0;
        public Shape()
        {
            colour = Color.Red;
        }


        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        public Shape( int x, int y)
        {
/*
            this.colour = colour;*/
            this.x = x;
            this.y = y;

        }


        /// <summary>
        /// Draw 
        /// </summary>
        /// <param name="g"></param>
        /// <param name="c"></param>
        /// <param name="thickness"></param>
        /// <param name="br"></param>
        public abstract void Draw(Graphics g, Color c, int thickness,Brush br);




        /// <summary>
        /// Select Color
        /// </summary>
        /// <param name="colour"></param>
        public virtual void SetColor(Color colour)
        {
            this.colour = colour;

        }
        /// <summary>
        /// Fill Color 
        /// </summary>
        /// <param name="colour"></param>
        public virtual void fillColor(Color colour)
        {
            this.colour = colour;

        }
        /// <summary>
        /// Set VAlue of x
        /// </summary>
        /// <param name="x"></param>
        public void SetX(int x)
        {
            this.x = x;
        }
        /// <summary>
        /// Set Value of y
        /// </summary>
        /// <param name="y"></param>
        public void SetY(int y)
        {
            this.y = y;
        }

        /// <summary>
        /// Get Value of x
        /// </summary>
        /// <returns></returns>
        public int GetX()
        {
            return x;
        }
        /// <summary>
        /// Get Value of y
        /// </summary>
        /// <returns></returns>
        public int GetY()
        {
            return y;
        }
        /// <summary>
        /// Overridden function for string.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return base.ToString() + "    " + this.x + "," + this.y + " : ";
        }
    }
}



