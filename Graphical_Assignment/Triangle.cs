﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Graphical_Assignment
{
    class Triangle :Shape
    {
        /// <summary>
        /// Declare variable
        /// </summary>
        PointF p1, p2, p3;

        /// <summary>
        /// Function to set point value
        /// </summary>
        /// <param name="point1"></param>
        /// <param name="point2"></param>
        /// <param name="point3"></param>
        /// <param name="point4"></param>
        /// <param name="point5"></param>
        /// <param name="point6"></param>
        public void setPoints(int point1, int point2, int point3, int point4, int point5, int point6)
        {
            this.p1 = new PointF(point1, point2);
            this.p2 = new PointF(point3, point4);
            this.p3 = new PointF(point5, point6);
        }

        /// <summary>
        /// Function to Draw 
        /// </summary>
        /// <param name="g"></param>
        /// <param name="c"></param>
        /// <param name="thickness"></param>
        /// <param name="br"></param>
        public override void Draw(Graphics g, Color c, int thickness,Brush br)
        {

            Pen p = new Pen(c, thickness);
            PointF[] curvPoints =
            {
                p1,p2,p3
            };
            g.DrawPolygon(p, curvPoints);
          //  g.FillPolygon(br, curvPoints);

        }
    }
}
