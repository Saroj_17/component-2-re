﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Graphical_Assignment
{
    public partial class Command : Form
    {
        public Command()
        {
            InitializeComponent();
            

        }
        // declare var
        Color c;
        int moveX, moveY;
        int thickness;
        Boolean drawCircle;
        Boolean drawRectangle;
        Boolean drawSquare;
        Boolean drawTriangle;

        Bitmap bitmapvalue = new Bitmap(600, 990);


        Color fill = Color.Transparent;
        

        int loopSize;
        Boolean loopstart = false;

        Boolean error = false;
       




        //make list|array to create obj
        List<Circle> circleObjects;
        List<Rectangle> rectangleObjects;
        List<Square> squareObjects;
        List<Triangle> triangleObjects;

        private void button1_Click(object sender, EventArgs e)
        {

            if (txtCmd.Text =="")
            {
                MessageBox.Show("Command line cannot be null!!");
            }
            else if (txtInput.Text =="")
            {
                MessageBox.Show("Code Cannot be null!!");
            }
            else
            {
                string cmm = txtCmd.Text.ToLower();
                string cod =txtInput.Text.ToLower();


                cmdParcer cmd = new cmdParcer();
                string parsedCode = cmd.ParserClass(cmm, cod);
                

                Graphics g = Graphics.FromImage(bitmapvalue);
                g.DrawImageUnscaled(cmd.GetBitmap(), 0, 0);
                pcDrawing.Refresh();
                
            }



        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
           
                
                
            
        }
        

        
        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            //draw shapes using bitmap
            Graphics g = e.Graphics;
            Brush br = new SolidBrush(System.Drawing.Color.Red);
            
            g.DrawImageUnscaled(bitmapvalue, 0, 0);



            /*
                        if (drawCircle == true)
                        {
                            foreach (Circle circleObject in circleObjects)
                            {
                                circleObject.Draw(g, c, thickness,br);
                            }
                        }
                        if (drawRectangle == true)
                        {
                            foreach (Rectangle rectangleObject in rectangleObjects)
                            {
                                rectangleObject.Draw(g, c, thickness,br);
                            }
                        }
                        if (drawSquare == true)
                        {
                            foreach (Square squareObject in squareObjects)
                            {
                                squareObject.Draw(g, c, thickness,br);
                            }
                        }
                        if (drawTriangle == true)
                        {
                            foreach (Triangle triangleObject in triangleObjects)
                            {
                                triangleObject.Draw(g, c, thickness,br);
                            }
                        }
            */

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            //to load text from file
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Text Files|*.txt";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                txtInput.Text = System.IO.File.ReadAllText(ofd.FileName);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //to save text in files
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Text Files|*.txt";

            if(sfd.ShowDialog()== DialogResult.OK)
            {
                System.IO.File.WriteAllText(sfd.FileName, txtInput.Text);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Command_Load(object sender, EventArgs e)
        {
            //load object while loading interface
           
            circleObjects = new List<Circle>();
            rectangleObjects = new List<Rectangle>();
            squareObjects = new List<Square>();
            triangleObjects = new List<Triangle>();
            c = Color.Black;
            thickness = 2;
        }
    }
}


