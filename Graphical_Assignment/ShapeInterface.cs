﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Graphical_Assignment
{
    interface ShapeInterface
    {
        /// <summary>
        /// declare interfaces for color
        /// </summary>
        /// <param name="cl"></param>
        void SetColor(Color cl);

        /// <summary>
        /// declare interface for Xvalue
        /// </summary>
        /// <param name="x"></param>
        void SetX(int x);
        /// <summary>
        /// declare interface for X.
        /// </summary>
        /// <returns></returns>
        int GetX();

        /// <summary>
        /// Interface for y value
        /// </summary>
        /// <param name="y"></param>
        void SetY(int y);
        /// <summary>
        /// Interface to get Y value
        /// </summary>
        /// <returns></returns>
        int GetY();
        /// <summary>
        /// Interface to draw Object 
        /// </summary>
        /// <param name="g"></param>
        /// <param name="c"></param>
        /// <param name="thick"></param>
        /// <param name="br"></param>
        void Draw(Graphics g, Color c, int thick,Brush br);

        


    }
}
